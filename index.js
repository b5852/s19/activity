console.log("Hello World");

// 3. Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
let num = 2;
let getCube = num ** 3;

// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
console.log(`The cube of ${num} is ${getCube}`);

// 5. Create a variable address with a value of an array containing details of an address.
let address = ["258", "Washington Ave NW", "California 90011"];
/*
	console.log(address[0]); //258
	console.log(address[1]); //Washington Ave NW
	console.log(address[2]); //California 90011
*/

// 6. Destructure the array and print out a message with the full address using Template Literals.
console.log(`I live at ${address[0]} ${address[1]}, ${address[2]}`);

// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
let animal = {
	animalName: "Lolong",
	animalDescription: "Saltwater Crocodile",
	animalWeight: "1075 kg",
	animalHeight: "20 ft 3 in"
};

// 8. Destructure the object and print out a message with the details of the animal using Template Literals.
let {animalName, animalDescription, animalWeight, animalHeight} = animal;

console.log(`${animalName} was a ${animalDescription}. He weighed ${animalWeight} with a measurement of ${animalHeight}.`)

// 9. Create an array of numbers.
let numbers = [1, 2, 3, 4, 5];

/*
	10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
*/
let loop = numbers.forEach(number => console.log(number));

/*
	11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
*/
let reduceNumber = [1, 2, 3, 4, 5]; 
const sum = reduceNumber.reduce((accumulator, currentValue) => { 
  return accumulator + currentValue;
});
console.log(sum);

/*
	12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
*/
class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

/*
	
	13. Create/instantiate a new object from the class Dog and console log the object.
*/
const iDog = new Dog();
iDog.name = "Frankie";
iDog.age = "5";
iDog.breed = "Maniature Dachshunnd";
	
console.log(iDog);













